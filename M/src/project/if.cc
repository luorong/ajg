#include <ros/ros.h>
#include <fstream>
#include <iostream>

#include <string>
#include <iostream>
#include <Eigen/Dense>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/PointCloud2.h>
#include <tf/transform_datatypes.h>
#include <boost/format.hpp>
#include <pcl/point_cloud.h>
#include <pcl/registration/icp.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <pcl/common/common.h>
 
 
ros::Publisher lidar_fusion_pub; 
 
// 坐标系转换矩阵定义
Eigen::Matrix4d lidar02local;
Eigen::Matrix4d local02utm;
 
Eigen::Matrix4d lidar12local;
Eigen::Matrix4d local12utm;
 
// 初始化矩阵
void initializeMatrices() {
    // 初始化坐标系转换矩阵
    lidar02local << 0.0476799789866, -0.998772610821, 0.0134123628391, 0.314027273074,
                  -0.12150167227, -0.0191271953828, -0.992406919581, -0.215657124688,
                  0.991445390952, 0.0456883165577, -0.122264526707, 6.45311085723,
                  0, 0, 0, 1;
 
    lidar12local << 0.0155631699082929, 0.997801586481612, -0.064418799718627, -0.0996553064905392,
                0.11405362334183, 0.062234528037919, 0.991523390809567, -0.181134083261809,
                0.993352685980668, -0.022778444518457, -0.112834319780437, 6.69058664245943,
                0, 0, 0, 1;              
                   
    local02utm << -0.942510399582, 0.334176819482, 0, 656453.78246,
                  -0.334176819482, -0.942510399582, 0.0, 2968417.21969,
                  0.0, 0.0, 1, 51.931,
                  0, 0, 0, 1;             
 
    local12utm << -0.94251039958223, 0.334176819482354, 0.0, 656453.782460093,
                -0.334176819482354, -0.94251039958223, 0, 2968417.21969498,
                0.0, 0, 1, 51.931,
                0, 0, 0, 1;    
    ROS_INFO("矩阵初始化");
}
 
pcl::PointCloud<pcl::PointXYZI> processPointClouds(const pcl::PointCloud<pcl::PointXYZI>& cloud, Eigen::Matrix4d lidar2local, Eigen::Matrix4d local2utm) {
// 将点云从激光雷达坐标系转换到本地坐标系
    pcl::PointCloud<pcl::PointXYZI> transformed_cloud;
    pcl::transformPointCloud(cloud, transformed_cloud, lidar2local);
    ROS_INFO("本地坐标系transformed_cloud_0的点云数量: %lu", transformed_cloud.size());
 
      // 将转换后的点云从本地坐标系到UTM坐标系
    pcl::PointCloud<pcl::PointXYZI> utm_cloud;
    pcl::transformPointCloud(transformed_cloud, utm_cloud, local2utm);
    ROS_INFO("UTM坐标系utm_cloud_0: %lu", utm_cloud.size());
 
    // 从 local2utm 矩阵中提取平移向量
    Eigen::Vector3d lidar_translation(local2utm(0, 3), local2utm(1, 3), local2utm(2, 3));
     
    // 打印平移向量
    ROS_INFO("雷达坐标系相对于UTM坐标系的平移向量: (%f, %f, %f)", lidar_translation.x(), lidar_translation.y(), lidar_translation.z());
 
    // 遍历每个点，将UTM坐标系的点云减去平移量得到平移后的点云
    for (auto& point : utm_cloud.points) {
        point.x -= lidar_translation.x();
        point.y -= lidar_translation.y();
        point.z -= lidar_translation.z();
    }
 
    return utm_cloud;
}
 
void callback(const sensor_msgs::PointCloud2ConstPtr& lidar_0_msg,
              const sensor_msgs::PointCloud2ConstPtr& lidar_1_msg) {
                
    ROS_INFO("开始融合");
    // 将ROS消息转换为PCL点云
    pcl::PointCloud<pcl::PointXYZI> cloud_0;
    pcl::PointCloud<pcl::PointXYZI> cloud_1;
 
    pcl::fromROSMsg(*lidar_0_msg, cloud_0);
    pcl::fromROSMsg(*lidar_1_msg, cloud_1);
 
    // 将pcd 从雷达坐标系转换为UTM坐标系（处理）
    pcl::PointCloud<pcl::PointXYZI> utm_cloud_0 = processPointClouds(cloud_0, lidar02local, local02utm);
    pcl::PointCloud<pcl::PointXYZI> utm_cloud_1 = processPointClouds(cloud_1, lidar12local, local12utm);
 
    // 融合点云
    pcl::PointCloud<pcl::PointXYZI> fused_cloud;
    fused_cloud += utm_cloud_0;
    fused_cloud += utm_cloud_1;
    ROS_INFO("融合后的点云数量: %lu", fused_cloud.size());
    ROS_INFO("融合后的点云");
 
    // 转换为ros消息类型
    sensor_msgs::PointCloud2 fused_cloud_msg;
    pcl::toROSMsg(fused_cloud, fused_cloud_msg);
    fused_cloud_msg.header.frame_id = "/base_link";
 
    // 发布融合后的点云
    lidar_fusion_pub.publish(fused_cloud_msg);
    ROS_INFO("发布");
 
}
 
int main(int argc, char** argv) {
    ros::init(argc, argv, "prefusion_node");
    ros::NodeHandle nh;
 
    // 中文显示
    std::locale::global(std::locale(""));
     // 在代码开始处记录开始时间戳
    ros::Time start_time = ros::Time::now();
    // 初始化矩阵
    initializeMatrices();

   
 
    std::string lidar_0_topic = "/iv_points_0";
    std::string lidar_1_topic = "/iv_points_1";
 
    message_filters::Subscriber<sensor_msgs::PointCloud2> lidar_0_sub(nh, lidar_0_topic, 10);
    message_filters::Subscriber<sensor_msgs::PointCloud2> lidar_1_sub(nh, lidar_1_topic, 10);
 
    typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::PointCloud2, sensor_msgs::PointCloud2>
        MySyncPolicy;
 
    // 定义全局的发布者
    lidar_fusion_pub = nh.advertise<sensor_msgs::PointCloud2>("/lidar_all_points", 1);
 
    message_filters::Synchronizer<MySyncPolicy> sync(MySyncPolicy(1), lidar_0_sub, lidar_1_sub);
 
    sync.registerCallback(boost::bind(&callback, _1, _2));

    // 在代码结束处记录结束时间戳
    ros::Time end_time = ros::Time::now();

    // 计算代码的运行时间
    double elapsed_time = (end_time - start_time).toSec();
    ROS_INFO("代码运行时间：%f 秒", elapsed_time);
 
    ros::spin();
 
    return 0;
}